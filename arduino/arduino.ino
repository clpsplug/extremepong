//注意：一部のderayとprintは必ず最後に消す
int UD1p = 0;//UD:上下の座標
int LR2p = 0;//LR:左右の座標
int vib1p2p = 0;
int vib1p,vib2p,bot1p,bot2p,UD1,LR2,sum_data;
//vib:振動モータの動作に利用。bot:ボタン。押すと０を出力。通常時１

void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(2, INPUT_PULLUP);//degitalReadに対応
  pinMode(12, OUTPUT);
  pinMode(4, INPUT_PULLUP);
}

void loop() {
  UD1p = analogRead(A0);
  //LR1p = analogRead(A1);
  //UD2p = analogRead(A2);
  LR2p = analogRead(A3);
  /*Serial.writeと併用すると正しく値が出力されない可能性あり
  //Serial.print("UD1p = ");
  //Serial.println(UD1p, DEC);
  //Serial.print(", LR1p = ");
  //Serial.println(LR1p, DEC);
  //Serial.print("UD2p = ");
  //Serial.println(UD2p, DEC);
  //Serial.print(", LR2p = ");
  //Serial.println(LR2p, DEC);
  Serial.write(LR2p);
  */
  
  bot1p = digitalRead(2);//導通しているかどうかをreadする。よくわからんがとりあえずボタンを押したらbotは 0 になる
  bot2p = digitalRead(4);
  //delay(1000);//プリント時見やすくする用。完成時には消す
  
/***１ｐ、２ｐのスティック・ボタン操作***
   * 全て、０のとき動作しない（静止）
   * 1p上操作：二進数100000　 1p下操作：二進数10000
   * 2p上操作：二進数1000　2p下操作:二進数100
   * 1pボタン：二進数10  2pボタン：二進数１
   */
  if(UD1p<=20 && UD1p>=10){
    UD1 = 0;
  }else if(UD1p>20){
    UD1 = 32;    //1p上操作：二進数100000
  }else if(UD1p<10){
    UD1 = 16;  //1p下操作：二進数10000
  }
  //delay(100);

  if(LR2p>= 200 && LR2p <= 300){
    LR2 = 0;
  }else if(LR2p>300){
    LR2 = 8;//2p上操作：二進数1000
  }else if(LR2p<200){
    LR2 = 4;//2p下操作:二進数100
  }
  //delay(100); 

  bot1p = digitalRead(2);//導通しているかどうかをreadする。よくわからんがとりあえずボタンを押したらbotは 0 になる
  //ボタンを押したときに２を出力させる
  if(bot1p == 0){
    bot1p = 2;
  }else{
    bot1p = 0;
  }
  
  bot2p = digitalRead(4);
  //ボタンを押したときに１を出力させる
  if(bot2p == 0){
    bot2p = 1;
  }else{
    bot2p = 0;
  }
  
  sum_data = UD1 + LR2 + bot1p + bot2p;//processingに送る値。操作用の値を合計する

  //個々の出力値の確認用。完成時には消す
  Serial.write(UD1);
    //delay(500);
  Serial.write(LR2);
    //delay(500);
  Serial.write(bot1p);
    //delay(500);
  Serial.write(bot2p);

  //全ての和をprocessingにおくる。完成時はこれだけ残す
  Serial.write(sum_data);

  
  /***振動モータの操作***
   * vib1p2pにprocessing から値を読み込む。とりあえず　0,1,2,3を想定
   * 0: どちらもふるえない　1: 1pのみ震える　
   * 2: 2pのみ震える　3:　両方震える
   */
  if (vib1p2p % 2 == 1) {
    digitalWrite(13,1);
  }
  else {
    digitalWrite(13,0);
  }
  if (vib1p2p / 2 == 1) {
    digitalWrite(12,1);
  }
  else {
    digitalWrite(12,0);
  }
  //delay(200);
}

//振動モータの制御値をprocessingから読み込む
void serialEvent()
{
  if (Serial.available() > 0)
  {
    vib1p2p = Serial.read();
  }
}
