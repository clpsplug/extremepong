import processing.serial.*;
import processing.sound.*;
Serial port;  // Create object from Serial class

enum paddleMoveState {
  UP,
  DOWN,
  STILL,
  COUNT,
}

enum gameState{
 WAIT4ARDUINO,
 PLAYING,
 ENDED,
 COUNT
}

enum ballState{
   PLAYING,
   NOCHILL,
   DEAD1P, // 1p loses point
   DEAD2P, // 2p loses point
   WAITING, // buffer time after goal happens
   COUNT
}


paddleMoveState[] paddleState = new paddleMoveState[2];

gameState gamestate = gameState.WAIT4ARDUINO;

Ball ball = new Ball(this);
Lazer lazer = new Lazer();
Paddle[] paddles = new Paddle[2];

int[] points = new int[2];
int gamePoint = 1;
int cnt = 0;
int[] dmgCnt = new int[2];

ArrayList<Bullet> bullets = new ArrayList<Bullet>();
ArrayList<Particle> parts = new ArrayList<Particle>();

boolean[] isBulletKeyOn = new boolean[2];

Label[] scoreLabels = new Label[2];
Label[] arduinoStat = new Label[6];
Label wonLabel;
Label warningLabel;

AmmoBar[] ammobars = new AmmoBar[2];

SoundFile bounce;
SoundFile goal;
SoundFile explosion;
SoundFile shoot;
SoundFile siren;
SoundFile ballLaser;

SoundFile ArduinoWaitBGM;
SoundFile MainBGM;
SoundFile ResultBGM;

int num_ports;
boolean device_detected = false;
String[] port_list;
String detected_port = "";
boolean noArduino = false; // true if user doesn't want to use Arduino.


void setup() {
  size(800, 600); 
  frameRate(60);

  paddleState[0] = paddleMoveState.STILL;
  paddleState[1] = paddleMoveState.STILL;
  
  scoreLabels[1] = new Label(color(255,255,0),600,100);
  scoreLabels[0] = new Label(color(255,255,0),100,100);
  
  warningLabel = new Label(color(255,128,0), width/2-175,height/2);
  wonLabel = new Label(color(0,255,255), width/2-175,height/2);
  
  for (int i = 0; i < 6; i++) {
    arduinoStat[i] = new Label(color(255,255,255),60 * (i + 1),300);
  }
  
  for (int i = 0; i < 2; i++) {
      scoreLabels[i].setFont("DSEG14Classic-BoldItalic.ttf", 80);
  }
  for (int i = 0; i < 6; i++) {
      arduinoStat[i].setFont("DSEG14Classic-BoldItalic.ttf", 20);
  }
  warningLabel.setFont("DSEG14Classic-BoldItalic.ttf", 60);
  wonLabel.setFont("DSEG14Classic-BoldItalic.ttf", 60);
  
  ball.init();

  paddles[0] = new Paddle();
  paddles[1] = new Paddle();

  paddles[0].setPosition(30, 40);
  paddles[1].setPosition(width - 30, 80);
  
  points[0] = 0;
  points[1] = 0;
  
  dmgCnt[0] = 0;
  dmgCnt[1] = 0;

  for (int i = 0; i < 2; i++) {
    paddles[i].init();
    ammobars[i] = new AmmoBar(i+1);
  }
  
  bounce = new SoundFile(this, "bounce.wav");
  goal = new SoundFile(this, "Hit_Hurt.wav");
  explosion  = new SoundFile(this, "Explosion2.wav");
  shoot = new SoundFile(this, "shoot.wav");
  siren = new SoundFile(this, "siren3.wav");
  ballLaser = new SoundFile(this, "ballLaser.wav");
  ArduinoWaitBGM = new SoundFile(this, "ArduinoWait.wav");
  MainBGM = new SoundFile(this, "Main.wav");
  ResultBGM = new SoundFile(this, "Result EXC.mp3");
  MainBGM.amp(0.8);
  
  printArray(Serial.list());
    
  // get the number of detected serial ports
  num_ports = Serial.list().length;
  // save the current list of serial ports
  port_list = new String[num_ports];
  for (int i = 0; i < num_ports; i++) {
    port_list[i] = Serial.list()[i];
  }

  ArduinoWaitBGM.loop();
  
}

void draw() {
  switch (gamestate){
    case WAIT4ARDUINO :
    {
      wait4arduinoDraw();
     break;
    }
    case PLAYING :
    {
      playingDraw();
      break;
    }
    case ENDED :
    {
      endedDraw();
    }
    default:
    {
    }
  }
      
  
}

void wait4arduinoDraw() {
    background(0);
    String[] revcur = new String[5];
    revcur[0]="-";revcur[1]="\\";revcur[2]="|";revcur[3]="/";
    // display instructions to user
    textFont(createFont("DSEG14Classic-BoldItalic.ttf", 80), 32);
    fill(color(0,255,0));
    text("EXTREMEPONG",250, 50);
    textFont(createFont("Lucida Console", 80), 30);
    fill(color(192+64*sin(2*PI/100*frameCount),192+64*sin(2*PI/100*frameCount),0));
    text(revcur[(frameCount%16)/4] + "...WAITING FOR CONTROLLER..." + revcur[(frameCount%16)/4], 125, 100);
    textFont(createFont("Meiryo", 80), 22);
    fill(color(255,255,255));
    text("1. 専用コントローラがすでに接続されている場合は、切断したあと、", 20, 150);
    text("      アプリを再起動してください。", 20, 200);
    text("2. 専用コントローラをUSBポートに接続してください。", 20, 250);
    text("3. 専用コントローラを使用しない場合は、Enterキーを押してください。", 20, 300);
    textFont(createFont("Lucida Console", 80), 16);
    text("Technical Info:", 20, 320);
    text("FPS: " + String.format("%2.0f", frameRate), 20, 340);
    
    // see if Arduino or serial device was plugged in
    if ((Serial.list().length > num_ports) && !device_detected) {
        device_detected = true;
        // determine which port the device was plugge into
        boolean str_match = false;
        if (num_ports == 0) {
            detected_port = Serial.list()[0];
        }
        else {
            for (int i = 0; i < Serial.list().length; i++) {  // go through the current port list
                for (int j = 0; j < num_ports; j++) {             // go through the saved port list
                    if (Serial.list()[i].equals(port_list[j])) {
                        break;
                    }
                    if (j == (num_ports - 1)) {
                        str_match = true;
                        detected_port = Serial.list()[i];
                    }
                }
            }
        }
    }
    // calculate and display serial port name
    if (device_detected) {
        ArduinoWaitBGM.stop();
        fill(color(random(192,255),random(192,255),random(192,255)));
        textFont(createFont("Meiryo", 80), 30);
        text("Arduino Engaged! GET READY TO PONG!", 20, 380);
        textFont(createFont("Lucida Console", 80), 18);
        text("Technical Information", 20, 430);
        text("Arduino Port Name:" + detected_port, 20, 480);
        text("Will attempt to connect to port in " + String.format("%d", 250-cnt) + "fr.", 20, 460);
        cnt++;
        if (cnt == 250){
           cnt = 0;
           gamestate = gameState.PLAYING;
           ball.state = ballState.WAITING;
           port = new Serial(this, detected_port, 9600);
           port.clear();
           MainBGM.loop();
        }
    }
    
    if (noArduino && !device_detected) {
      ArduinoWaitBGM.stop();
      textFont(createFont("Meiryo", 80), 30);
      fill(color(random(192,255),random(192,255),random(192,255)));
      text("GET READY TO PONG!", 20, 380);
      cnt++;
      if (cnt == 250){
        cnt = 0;
        gamestate = gameState.PLAYING;
        ball.state = ballState.WAITING;
        MainBGM.loop();
      }
    }

}

void playingDraw() {
  initDrawing();

  //println(ball.state);
  // if ball is in play... do a collision!
  switch (ball.state) {
    case NOCHILL: 
    {
      if (cnt < 150) {
        if (cnt % 4 < 2) {
          warningLabel.setText("WARNING");
        }
        else {
          warningLabel.setText("");
        }
      }
      else {
        if (cnt == 150){
         ball.speedUp(1.5f); 
        }
        warningLabel.setText("");        
        if (cnt % 50 == 0) {
          lazer.turnOn();
          ballLaser.play();
        }
        if (cnt == 300) {
          cnt = 0;
          ball.state = ballState.PLAYING;
       }
       // println("THE BALL HAS NO CHILL!");
      }
      // Falling through into PLAYING
      cnt++;
    }
    case PLAYING : 
    {
      //warningLabel.setText("");
      // Check for collision
      // keep in mind that ball's position is the center of the ball
      float ballX = ball.getPosition()[0];
      float bally = ball.getPosition()[1];

      // is the paddle in the range of the left side to the center of the ball?
      if ( ballX - ball.getSize() / 2 < paddles[0].getPosition()[0] + paddles[0].getWidth() && ballX > paddles[0].getPosition()[0] + paddles[0].getWidth()) {
        //println("P1 Collision?");
        if (bally > paddles[0].getPosition()[1] && bally < paddles[0].getPosition()[1] + paddles[0].getHeight()) {
          ball.collision(-paddles[0].edgeCheck(bally), paddles[0].getPosition()[0], true);
          bounce.play();
        }
      }

      if ( ballX < paddles[1].getPosition()[0] && ballX + ball.getSize() / 2 > paddles[1].getPosition()[0]) {
        //println("P2 Collision?");
        if (bally > paddles[1].getPosition()[1] && bally < paddles[1].getPosition()[1] + paddles[1].getHeight()) {
          ball.collision(paddles[1].edgeCheck(bally), paddles[1].getPosition()[0], false);
          bounce.play();
        }
      }
      
      // Check for bullet collision
      if (bullets.size() > 0) {
        for (int i = 0; i < bullets.size(); i++) {
          // keep in mind that ball's position is the center of the ball
          float bulletX = bullets.get(i).getPosition()[0];
          float bulletY = bullets.get(i).getPosition()[1]; 
          float bulletSize = bullets.get(i). getSize();
          // is the paddle in the range of the bullet?
          if (paddles[0].getPosition()[0] < bulletX + bullets.get(i).getSize() && bulletX < paddles[0].getPosition()[0] + paddles[0].getWidth()) {            
            if (paddles[0].getPosition()[1] < bulletY + bullets.get(i).getSize() && bulletY < paddles[0].getPosition()[1] + paddles[1].getHeight()) {
              // Damaged
              println("P1 Damaged");
              explosion.play();
              paddles[0].damage();
              bullets.get(i).setDead();
              dmgCnt[0] = 10;
              addParts(bulletX,bulletY,20);
            }
          }  
          if (paddles[1].getPosition()[0] < bulletX + bullets.get(i).getSize() && bulletX < paddles[1].getPosition()[0] + paddles[1].getWidth()) {
            if (paddles[1].getPosition()[1] < bulletY + bullets.get(i).getSize() && bulletY < paddles[1].getPosition()[1] + paddles[1].getHeight()) {
              println("P2 Damaged");
              explosion.play();
              paddles[1].damage();
              bullets.get(i).setDead();
              dmgCnt[1] = 10;
              addParts(bulletX,bulletY,20);
            }
          }
          // Bullet vs Ball
          if (bulletX<ball.getPosition()[0]&&ball.getPosition()[0]<bulletX+bulletSize) {
            if ( bulletY<ball.getPosition()[1]&&ball.getPosition()[1]<bulletY+bulletSize){
              if (ball.state == ballState.PLAYING) {
                ball.state = ballState.NOCHILL;
                cnt = 0;
                siren.play();
                addParts(bulletX,bulletY,50);
              }
            }
          }
        }
      }
      
      // Check for Lazer COllision
      if ( lazer.getFrame() < 6 ) {
       // if there is the lazer...check for it!
       //1p
       if (paddles[0].getPosition()[1] < ball.getPosition()[1] && ball.getPosition()[1] < paddles[0].getPosition()[1] + paddles[0].getHeight()) {
        // Damaged
        println("P1 Damaged from Lazer");
        explosion.play();
        paddles[0].damage();
        dmgCnt[0] = 10;
        addParts(ball.getPosition()[0], ball.getPosition()[1],5);
       }
       //2p
       if (paddles[1].getPosition()[1] < ball.getPosition()[1] && ball.getPosition()[1] < paddles[1].getPosition()[1] + paddles[1].getHeight()) {
        // Damaged
        println("P2 Damaged from Lazer");
        explosion.play();
        paddles[1].damage();
        dmgCnt[1] = 10;
        addParts(ball.getPosition()[0], ball.getPosition()[1],5);
       }
      }
        
      
      if (bullets.size() > 0) {
        for (int i = bullets.size() - 1; i >= 0; i--) {
          bullets.get(i).update(paddles[0].getPosition()[0],paddles[0].getPosition()[1]);
        }
      }
      
      //recover ammo
      for (int i = 0; i < 2; i++){
        ammobars[i].update(); 
      }
          
      break;
    }
    case DEAD1P :
    {
      points[0] ++;
      cnt = 0;
      ammobars[1].addAmmo(1);
      ball.state = ballState.WAITING;
      goal.play();
      break;
    }
    case DEAD2P :
    {
      points[1] ++;
      cnt = 0;
      ammobars[0].addAmmo(1);
      ball.state = ballState.WAITING;
      goal.play();
      break;
    }
    case WAITING :
    {
      if (!(points[0] == gamePoint || points[1] == gamePoint)) {
        if (cnt % 4 < 2) {
          warningLabel.setText("GET READY");
        }
        else {
          warningLabel.setText("");
        }
      }
      if (cnt == 150) {
        if ((points[0] == gamePoint || points[1] == gamePoint)) {
          gamestate = gameState.ENDED;
          cnt = 0;
          MainBGM.stop();
          ResultBGM.loop();
          break;
        }
        ball.init();
        ball.state = ballState.PLAYING;
        for(int i = 0; i < 2; i++) {
         paddles[i].init(); 
        }
        if (bullets.size() > 0) {
          for (int i = bullets.size() - 1; i >= 0; i--) {
            bullets.remove(i);
          }
        }
        cnt = 0;
        break;
      }
      cnt++;
      break;
    }
    default :
    {
    }
    
  }
  

  
  // Do an update
  ball.update();
  ball.draw();
  lazer.update();
  lazer.draw(ball.getPosition()[1]);
  warningLabel.draw();
  
  // Do player mumbo jumbo - Draw Paddles and ammobars, deduct dmgcnt...
  for (int i = 0; i < 2; i++) {
    paddles[i].draw();
    ammobars[i].draw();
    dmgCnt[i] --;
  }

  // Draw Bullets
  if (bullets.size() > 0) {
    for (int i = bullets.size() - 1; i >= 0; i--) {
      bullets.get(i).draw();
      if (bullets.get(i).getDead()) {
        //println("bullets out of screen");
        bullets.remove(i);
      }
    }
  }

  if (parts.size() > 0) {
    for (int i = parts.size() - 1; i >= 0; i--) {
      parts.get(i).update();
      parts.get(i).draw();
      if (parts.get(i).getDead()) {
        //println("bullets out of screen");
        parts.remove(i);
      }
    }
  }


  // move Paddles
  for (int i = 0; i < 2; i++) {
    float[] curPos = new float[2];
    curPos = paddles[i].getPosition();
    switch (paddleState[i]) {
    case UP :
      {
        paddles[i].setPosition(curPos[0], curPos[1]-10);
        break;
      }
    case DOWN : 
      {
        paddles[i].setPosition(curPos[0], curPos[1]+10);
        break;
      }
    }
    if (paddles[i].getPosition()[1] < 0) {
      paddles[i].setPosition(curPos[0], 0);
    }
    if (paddles[i].getPosition()[1] > height - paddles[i].getHeight()) {
      paddles[i].setPosition(curPos[0], height - paddles[i].getHeight());
    }
  }
  
  // Show the scores
  
  for (int i = 0; i < 2; i++) {
    scoreLabels[i].setText(String.format("%02d",points[i]));
    scoreLabels[i].draw();
  }
  //for (int i = 0; i < 6; i++) {
    //arduinoStat[i].draw();
  //}
}


// Input Checker
void keyPressed() {

  if (keyCode == ENTER) {
    noArduino = true;
  }
  switch (gamestate) { 
    case PLAYING:
    {
      if (keyCode == UP) { 
        paddleState[0] = paddleMoveState.UP;
      }
      if (keyCode == DOWN) {
        paddleState[0] = paddleMoveState.DOWN;
      }
      if (key == 'w' || key == 'W') { 
        paddleState[1] = paddleMoveState.UP;
      }
      if (key == 's' || key == 'S') {
        paddleState[1] = paddleMoveState.DOWN;
      }
      if (keyCode == SHIFT) {
        if (!isBulletKeyOn[0] && ammobars[0].useAmmo(1)){
          Bullet bullet = new Bullet();
          bullet.init(paddles[0].getPosition()[0]+paddles[0].getWidth(), paddles[0].getPosition()[1] + paddles[0].getHeight()/2,-PI/4);
          bullets.add(bullet);
          isBulletKeyOn[0] = true;
          shoot.play();
        }
      }
      if (key == 'd' || key == 'D') {
        if (!isBulletKeyOn[1] && ammobars[1].useAmmo(1)){
          Bullet bullet = new Bullet();
          bullet.init(paddles[1].getPosition()[0]-bullet.getSize(), paddles[1].getPosition()[1] + paddles[1].getHeight()/2,PI/4*5);
          bullets.add(bullet);
          isBulletKeyOn[1] = true;
          shoot.play();
        }
      }
      break;
    }
    case ENDED:
    {
     if (keyCode == SHIFT) {
      cnt = 0;
      gamestate = gameState.PLAYING;
      ball.state = ballState.WAITING;
      MainBGM.loop();
      ResultBGM.stop();
      points[0] = 0; points[1] = 0;
     }
     if (key == 'd' || key == 'D') {
      cnt = 0;
      gamestate = gameState.PLAYING;
      ball.state = ballState.WAITING;
      MainBGM.loop();
      ResultBGM.stop();
      points[0] = 0; points[1] = 0;
      }
    }
  }
}

void keyReleased() {

  if (keyCode == UP || keyCode == DOWN) { 
    paddleState[0] = paddleMoveState.STILL;
  }
  if (key == 'w' || key == 's' || key == 'W' || key == 'S') { 
    paddleState[1] = paddleMoveState.STILL;
  }
  if (keyCode == SHIFT) {
    isBulletKeyOn[0] = false;
  }
  if (key == 'd' || key == 'D') {
   isBulletKeyOn[1] = false; 
  }
}

void serialEvent(Serial port)
{  
  if (port.available() > 5)
  {
    /*x = port.read() + port.read() * 256;
    y = port.read() + port.read() * 256;
    z = port.read() + port.read() * 256;
    */
    for (int i = 0; i < 4; i ++){
      port.read(); //初めの4つは捨てる
      //arduinoStat[i].setText(String.format("%d",port.read()));
    }
    
    int control;
    
    control = port.read();
    
    println(control);
    switch (gamestate) {
      case PLAYING:{
        if ((control & 32) == 32) {
          paddleState[0] = paddleMoveState.UP;
        }
        else if ((control & 16) == 16) {
          paddleState[0] = paddleMoveState.DOWN;
        }
        else {
          paddleState[0] = paddleMoveState.STILL;
        }
        if ((control & 8) == 8) {
          paddleState[1] = paddleMoveState.UP;
        }
        else if ((control & 4) == 4) {
          paddleState[1] = paddleMoveState.DOWN;
        }
        else {
          paddleState[1] = paddleMoveState.STILL;
        }
        if ((control & 2) == 2) {
          if (!isBulletKeyOn[0] && ammobars[0].useAmmo(1)){
            Bullet bullet = new Bullet();
            bullet.init(paddles[0].getPosition()[0]+paddles[0].getWidth(), paddles[0].getPosition()[1] + paddles[0].getHeight()/2,-PI/4);
            bullets.add(bullet);
            isBulletKeyOn[0] = true;
            shoot.play();
          }
        }
        else {
          isBulletKeyOn[0] = false; 
        }
        if ((control & 1) == 1) {
          if (!isBulletKeyOn[1] && ammobars[1].useAmmo(1)){
            Bullet bullet = new Bullet();
            bullet.init(paddles[1].getPosition()[0]-bullet.getSize(), paddles[1].getPosition()[1] + paddles[1].getHeight()/2,PI/4*5);
            bullets.add(bullet);
            isBulletKeyOn[1] = true;
            shoot.play();
          }
        }
        else {
          isBulletKeyOn[1] = false; 
        }
        break;
      }
      case ENDED:
      {
        if ((control & 2) == 2) {
          cnt = 0;
          gamestate = gameState.PLAYING;
          ball.state = ballState.WAITING;
          MainBGM.loop();
          ResultBGM.stop();
          points[0] = 0; points[1] = 0;
        }
        if ((control & 1) == 1) {
          cnt = 0;
          gamestate = gameState.PLAYING;
          ball.state = ballState.WAITING;
          MainBGM.loop();
          ResultBGM.stop();
          points[0] = 0; points[1] = 0;
        }
      }
    }
    int dmgSerial;
    
    dmgSerial = (dmgCnt[0] > 0 ? 1 : 0) + (dmgCnt[1] > 0 ? 2 : 0);
    
    port.write(dmgSerial);
  }
}

void endedDraw() {
  initDrawing();
  
  if (parts.size() > 0) {
    for (int i = parts.size() - 1; i >= 0; i--) {
      parts.get(i).update();
      parts.get(i).draw();
      if (parts.get(i).getDead()) {
        //println("bullets out of screen");
        parts.remove(i);
      }
    }
  }
  
  // Show the scores
  
  for (int i = 0; i < 2; i++) {
    scoreLabels[i].setText(String.format("%02d",points[i]));
    scoreLabels[i].draw();
    ammobars[i].reset();
  }
  
  if (points[0] == gamePoint) {
    wonLabel.setText("P1 WINS"); 
  }
  else {
    wonLabel.setText("P2 WINS");
  }
  cnt++;
  wonLabel.draw();
  if (cnt == 68*60) {
    gamestate = gameState.PLAYING; 
    cnt = 0;
  }
  
  if (cnt % 5 == 0) {
   addParts(random(0,width), random(0, height), 50); 
  }
  
}
void initDrawing() {
  background(0, 0, 0);
}

void addParts(float x, float y, int qty) {
 for (int i = 0; i < qty; i++) {
   Particle part = new Particle(x,y);
   parts.add(part);
 }
}