class Particle {
  
  int initDuration = 30;
  float initSpeed = 10.0f;
  float x;
  float y;
  float size = 5.0f;
  int duration;
  float speed;
  float angle;
  color c;
  
  Particle(float initX, float initY) {
    x = initX;
    y = initY;
    angle = 2*PI/100*random(0,100);
    c = color(random(128,255),random(128,255),random(128,255));
    println(angle);
    speed = initSpeed;
    duration = initDuration;
  }
  
  void update() { 
    x += speed * cos(angle);
    y += speed * sin(angle);
    duration--;
    speed -= initSpeed/initDuration;
  }
  
  void draw() {
    fill(c);
    rect(x,y,size,size);
  }
  
  boolean getDead() {
    return duration == 0;
  }
  
  
}