class Label {
  
  
  // Member Variables
  // Color
  color c1 = color(255, 255, 255);
  String content = "";
  int x = 0;
  int y = 0;
  PFont font;
  // Text (to show)
  
  Label(color fontColor, int _x, int _y) {
    
    c1 = fontColor;
    x = _x;
    y = _y;
    
  }
  
  // Methods
  // void draw() to show the variable Text
  void draw(){
    fill(c1);
    textFont(font);
    text(content, x, y);
  }
  
  // void setText(String value) to change the variable Text
  void setText(String value){
    content = value;
  }
  // void setColor(int r, int g, int b) to change color
  //color c1 = color(255, 255, 255);
  void setColor(int r, int g, int b){
    c1 = color(r, g, b);
  }
  
  void setPosition(int _x, int _y){  
    x = _x;
    y = _y;
  }
  
  void setFont(String name, float size){
    font = createFont(name, size);
    
      
  }
  
}