class Bullet {
  
  float x;
  float y;
  float angle = PI*2 /8; //Radian
  float speed = 4.0f;
  float size = 15.0f;
  float[] paddleData = new float[2];
  boolean dead = false;
    
  void init(float _x, float _y, float _angle)
  {
    x = _x;
    y = _y;
    angle = _angle;
  }
  
  void draw()
  {
    fill(color(255,0,0));
    rect(x, y, size, size); 
  }
  
  void update(float _x, float _y)
  {
    paddleData[0] = _x;
    paddleData[1] = _y;
    
    x += speed * cos(angle);
    y += speed * sin(angle);
    
    if (x < 0) {
      dead=true;
    }
    if (x > width) {
      dead=true;
    }
    
    if (y < 0) {
      flipY();
    }
    if (y > height) {
      y = height;
      flipY();
    }
    
  }
  
  void flipY () {
     
    //Range of atan is -pi/2 ~ pi/2 !!!!! (1st or 4th zone)
    
    // Check which zone angle is in
    if (angle >= 0 && angle < PI / 2) {
      // 1st zone (x+, y+). atan goes to 4th zone. No fix.
      angle = atan(-sin(angle) / cos(angle)); 
      println("1st zone y-reflection occurred");
    }
    else if (angle >= PI / 2 && angle < PI) {
      //2nd zone (x-, y+). atan goes to 1st zone. Should be going to 3rd zone.
      angle = PI+atan(-sin(angle) / cos(angle));
      println("2nd zone y-reflection occurred");
    }
    else if (angle >= PI && angle < PI * 3 / 2) {
      //3rd zone (x-, y-). atan goes to 4th zone. Should be reflected into 2nd zone.
      angle = PI + atan(-sin(angle) / cos(angle));
      println("3rd zone y-reflection occurred");
    }
    else {
      //4th zone (x+, y-). atan goes to 1st zone. No fix.
      angle = atan(-sin(angle) / cos(angle));
      println("4th zone y-reflection occurred");
    }
  }
  
  float[] getPosition() {
    float position[] = new float[2];
    position[0] = x;
    position[1] = y;
    return position;
  }
  void collision()
  {
    
  }
  
  boolean getDead(){
    return dead;
  }
  
  void setDead() {
    dead = true;
  }
  
  float getSize() {
    return size;
  }
  
}