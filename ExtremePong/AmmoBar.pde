class AmmoBar {
  
 float ammo=0.0f;
 int player;
 
  AmmoBar(int _player) {
   
    player = _player;
    
  }
 
  void draw() {
    
    switch (player) {
     case 1 :
     {
       if (ammo < 1.0f) {
         fill(color(128,0,0));
         rect(50,550,40*ammo,20,10,0,0,10);
       }
       else {
         for (int i = 0; i < floor(ammo); i++) {
           fill(color(255,0,0));
           rect(50+50*i,550,40,20,10,10,10,10);
         }
         fill(color(128,0,0));
         rect(50+50*floor(ammo),550,40*(ammo-floor(ammo)),20,10,0,0,10);
       }
       break;
     }
     case 2 :
     {
       if (ammo < 1.0f) {
         fill(color(0,64,128));
         rect(width-90+40*(1-ammo),550,40*ammo,20,0,10,10,0);
       }
       else {
         for (int i = 0; i < floor(ammo); i++) {
           fill(color(0,128,255));
           rect(width-90-50*i,550,40,20,10,10,10,10);
         }
         fill(color(0,64,128));
         rect(width-90-50*floor(ammo)+40*(1-(ammo-floor(ammo))),550,40*(ammo-floor(ammo)),20,0,10,10,0);
       }
       break;
     }
     default :
     {
       println("Warning: invalid player number");
     }
      
      
    }
  
  }
  
  void update() {
    
   ammo += 0.005; 
   if (ammo > 5.0f) {
      ammo = 5.0f; 
   }

  }
  
  void addAmmo(int value) {
   ammo += value; 
   if (ammo > 5.0f) {
      ammo = 5.0f; 
   }
  }
  
  boolean useAmmo(int value) {

     if (ammo < value) {
        return false; 
     }
     ammo -= value; 
     return true;
  }
   
  void reset() {
    ammo = 0; 
  }
    
}
    