public class Paddle {
  
  PImage img;
  int size_x = 12;  
  int size_y = 100;
  float x;
  float y;
 
  
  void init() {
    //img = loadImage("");
    size_x = 12;
    size_y = 100;
  }
  
  void draw() {
    fill(color(255,255,255));
    rect(x, y, size_x, size_y); 
  }
  
  void setPosition(float _x, float _y) {
   x = _x;
   y = _y;
  }
  
  float[] getPosition() {
    float position[] = new float[2];
    position[0] = x;
    position[1] = y;
    return position;
  }
  
  int getWidth() {
    return size_x;
  }
  
  int getHeight() {
    return size_y;
  }
  
  void damage() {
    
    size_y -= 15; 
    
    if (size_y < 40) {
      size_y = 40; 
    }
    
  }
 
  int edgeCheck(float y_axis) {
    
   if (y_axis > y && y_axis < y + getWidth() / 4) {
     return 1;
   }
   else if (y_axis > y + getWidth() * 3 / 4 && y_axis < y + getWidth()) {
     return -1;
   }
   else {
     return 0;
   }
    
  }
  
}