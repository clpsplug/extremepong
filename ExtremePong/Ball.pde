import processing.sound.*;

public class Ball {

  PImage img;
  int ball_size = 16;
  float x;
  float y;
  float angle = PI*2 /8; //Radian
  float speed = 5;
  ballState state = ballState.WAITING;
  PApplet mainInstance;
  SoundFile winBounce;
  
  Ball (PApplet main) {
    mainInstance = main;
  }
  
  int getSize() {
    return ball_size; 
  }

  void init() {
 
    //img=loadImage("");
    x = width / 2 + random(-50,50);
    y = 8;
    angle = PI * 2 / 8;
    speed = 5.0f;
    state = ballState.PLAYING;
    winBounce = new SoundFile(mainInstance, "windowBounce.wav");
    
  }

  void draw() {
    //image(img,x,y);
    fill(color(255,255,255));
    ellipse(x, y, ball_size, ball_size);
    speed += 0.001f;
  }
  
  float[] getPosition() {
    float position[] = new float[2];
    position[0] = x;
    position[1] = y;
    return position;
  }
    
  float getSpeed() {
    return speed;
  }
  void speedUp(float var) {
     speed +=var; 
  }
  void setSpeed(float var) {
    speed = var;
  }
    
  float getAngle() {
    return angle;
  }
  
  void update() {
   if (state == ballState.WAITING) return;
    x += speed * cos(angle);
    y += speed * sin(angle);
    if (x < 0) {
      /*
      flipX();
      angle = atan(sin(angle) / -cos(angle));
      */
      state = ballState.DEAD2P; // 2P Loses!
    }
    if (x > width) {
      // kind of dirty, but had to force x to be out of condition
      /*
      x = width;
      flipX();
      */
      state = ballState.DEAD1P;
    }
    if (y < 0) {
      flipY();
      winBounce.play();
    }
    if (y > height) {
      y = height;
      flipY();
      winBounce.play();
    }
    
  }
  
  void collision(int anglechange, float edgePosition, boolean isPlayer1) {
    
    flipX();

    if (anglechange > 0) {

        angle += PI / 16;

      if (int(degrees(angle)) % 90 == 0) {
      
        angle -= PI / 16;
      
      }
    
    }
    else {
      
        angle -= PI / 16;

      if (int(degrees(angle)) % 90 == 0) {
      
        angle += PI / 16;
      
      }

      
    }
    
      x = edgePosition + (isPlayer1 ? getSize()*1.1 : -getSize()); // without this the ball might get stuck inside the paddle.
      
      setSpeed(getSpeed() + 0.05f);
  }
  
  void flipX () {
     
    //Range of atan is -pi/2 ~ pi/2 !!!!! (1st or 4th zone)
    
    // Check which zone angle is in
    if (angle >= 0 && angle < PI / 2) {
      // 1st zone (x+, y+). atan goes to 4th zone. Should be reflected into 2nd zone.
      angle = PI + atan(sin(angle) / -cos(angle));
      println("1st zone x-reflection occurred");
    }
    else if (angle >= PI / 2 && angle < PI) {
      //2nd zone (x-, y+). atan goes to 4th zone. should be going to 1st zone.
      angle = +atan(sin(angle) / -cos(angle));
      println("2nd zone x-reflection occurred");
    }
    else if (angle >= PI && angle < PI * 3 / 2) {
      //3rd zone (x-, y-). atan goes to 1st zone. should be going to 4th zone.
      angle = -atan(sin(angle) / cos(angle));
      println("3rd zone x-reflection occurred");
    }
    else {
      //4th zone (x+, y-). atan goes to 1st zone. Should be reflected into 3rd zone.
      angle = PI + atan(sin(angle) / -cos(angle));
      println("4th zone x-reflection occurred");
    }
    
  }
  
    void flipY () {
     
    //Range of atan is -pi/2 ~ pi/2 !!!!! (1st or 4th zone)
    
    // Check which zone angle is in
    if (angle >= 0 && angle < PI / 2) {
      // 1st zone (x+, y+). atan goes to 4th zone. No fix.
      angle = atan(-sin(angle) / cos(angle)); 
      println("1st zone y-reflection occurred");
    }
    else if (angle >= PI / 2 && angle < PI) {
      //2nd zone (x-, y+). atan goes to 1st zone. Should be going to 3rd zone.
      angle = PI+atan(-sin(angle) / cos(angle));
      println("2nd zone y-reflection occurred");
    }
    else if (angle >= PI && angle < PI * 3 / 2) {
      //3rd zone (x-, y-). atan goes to 4th zone. Should be reflected into 2nd zone.
      angle = PI + atan(-sin(angle) / cos(angle));
      println("3rd zone y-reflection occurred");
    }
    else {
      //4th zone (x+, y-). atan goes to 1st zone. No fix.
      angle = atan(-sin(angle) / cos(angle));
      println("4th zone y-reflection occurred");
    }
  }
}